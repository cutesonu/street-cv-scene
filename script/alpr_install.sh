#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install -y openalpr openalpr-daemon openalpr-utils libopenalpr-dev python3-openalpr
sudo apt-get install openalpr-utils