import os
import sys
import cv2
import numpy as np
from openalpr import Alpr

from src.settings import KEY_CONFIDENCE, KEY_BOX, KEY_LABEL


_cur_dir = os.path.dirname(os.path.realpath(__file__))


class PlateDetect:

    def __init__(self, country='eu', region='md', debug=True):

        self.max_width = 300
        self.debug = debug
        self.alpr = Alpr(country=country,
                         config_file="/etc/openalpr/openalpr.conf",
                         runtime_dir='/usr/share/openalpr/runtime_data')

        if not self.alpr.is_loaded():
            sys.stderr.write("Error loading OpenALPR")
            sys.exit(1)

        self.alpr.set_top_n(2)
        self.alpr.set_default_region(region)

        self.roi_margin = 0.15

    def destroy(self):
        self.alpr.unload()

    def detect(self, img, roi=None):
        new_roi = roi
        if img is None or img.shape[0] == 0:
            return None, new_roi

        img_h, img_w = img.shape[:2]

        if img_w > self.max_width:
            img = cv2.resize(img, (self.max_width, img_h * self.max_width // img_w))
            img_h, img_w = img.shape[:2]

        if roi is not None:
            (rx1, ry1, rx2, ry2) = roi  # ratio on img
            x1 = int(max((rx1 - self.roi_margin), 0) * img_w)
            y1 = int(max((ry1 - self.roi_margin), 0) * img_h)
            x2 = int(min((rx2 + self.roi_margin), 1.0) * img_w)
            y2 = int(min((ry2 + self.roi_margin), 1.0) * img_h)
        else:
            [x1, y1, x2, y2] = ([0, 0, 1.0, 1.0] * np.array([img_w, img_h, img_w, img_h])).astype(np.uint)

        crop = img[y1:y2, x1:x2]
        # if self.debug:
        #     cv2.imshow("crop_plate", crop)
        #     cv2.waitKey(1)

        _, enc = cv2.imencode("*.bmp", crop)
        bytes_data = bytes((bytearray(enc)))

        results = self.alpr.recognize_array(bytes_data)

        plates = []
        for r in results['results']:
            coordinates = r['coordinates']
            float_pts = []

            li_x = []
            li_y = []
            for coordinate in coordinates:
                li_x.append(1.0 * (coordinate['x'] + x1) / img_w)
                li_y.append(1.0 * (coordinate['y'] + y1) / img_h)

            px1 = min(li_x)
            px2 = max(li_x)
            py1 = min(li_y)
            py2 = max(li_y)
            plates.append({
                KEY_LABEL: "plate",
                KEY_CONFIDENCE: r['candidates'],
                KEY_BOX: (px1, py1, px2, py2)
            })

        return plates
