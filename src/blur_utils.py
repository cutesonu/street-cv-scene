import cv2
import numpy as np
from src.settings import KEY_BOX


def blur(img, objects, type="rect"):
    im_h, im_w = img.shape[:2]
    mask_img = np.zeros_like(img.copy(), dtype=np.uint8)

    for obj in objects:
        x1, y1, x2, y2 = (obj[KEY_BOX] * np.array([im_w, im_h, im_w, im_h])).astype(np.uint).tolist()
        if type == "rect":
            mask_img[y1:y2, x1:x2] = (255, 255, 255)
        elif type == "circle":
            cx = (x1 + x2) // 2
            cy = (y1 + y2) // 2
            r = max(x2-x1, y2-y1)
            mask_img = cv2.circle(mask_img, (cx, cy), r, (255, 255, 255), -1)

    cv2.imshow("mask", mask_img)
    cv2.waitKey(0)

    blur_img = cv2.blur(img.copy(), ksize=(11, 11))

    mask_img = mask_img.astype(np.float)

    comb_img = img * (1 - mask_img / 255.0) + blur_img * mask_img / 255.0
    comb_img.astype(dtype=np.uint8)
    return comb_img
