import sys
import numpy as np
import cv2
import os

from src.settings import COCO_LABELS, COCO_COLORS, C_PERSON, KEY_LABEL, KEY_BOX, KEY_CONFIDENCE, MODEL_DIR


YOLO_V3_MODEL = [os.path.join(MODEL_DIR, 'yolo/yolo_v3', fn) for fn in ["yolov3.weights", "yolov3.cfg"]]

# load the COCO class labels our YOLO model was trained on
LABELS = COCO_LABELS
# initialize a list of colors to represent each possible class label
COLORS = COCO_COLORS
# target objects

CONFIDENCE_THRESH = 0.7
OVERLAP_THRESH = 0.3


class YoloUtils:
    def __init__(self, targets=C_PERSON, score_threshold=CONFIDENCE_THRESH,
                 overlap_threshold=OVERLAP_THRESH):
        sys.stdout.write("loading YOLO v3 model[configure and weight files]")
        if len(targets) == 0:
            self.targets = range(len(LABELS))
        else:
            self.targets = targets

        # [confidence]
        self.score_threshold = score_threshold

        # [threshol of overlap]
        self.overlap_threshold = overlap_threshold

        self.net = cv2.dnn.readNetFromDarknet(YOLO_V3_MODEL[1], YOLO_V3_MODEL[0])
        self.scale = 1 / 255.0
        self.sz = (608, 608)

        self.ln = self.net.getLayerNames()
        self.ln = [self.ln[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]

    def detect(self, img):
        blob = cv2.dnn.blobFromImage(image=img, scalefactor=self.scale, size=self.sz, swapRB=True, crop=False)
        self.net.setInput(blob)
        layer_outputs = self.net.forward(self.ln)

        boxes = []
        confidences = []
        label_ids = []

        for output in layer_outputs:

            for detection in output:
                scores = detection[5:]
                label_id = np.argmax(scores)
                confidence = scores[label_id]

                if confidence > self.score_threshold and label_id in self.targets:
                    (center_x, center_y, width, height) = detection[0:4]

                    x = center_x - (width / 2)
                    y = center_y - (height / 2)

                    boxes.append([x, y, width, height])
                    confidences.append(float(confidence))
                    label_ids.append(label_id)

        return self.__rearrange_detects(img_sz=img.shape[:2], boxes=boxes, confidences=confidences, label_ids=label_ids)

    def __rearrange_detects(self, img_sz, boxes, confidences, label_ids):
        img_h, img_w = img_sz
        for i, box in enumerate(boxes):
            boxes[i] = (boxes[i] * np.array([img_w, img_h, img_w, img_h])).astype(np.int)

        idxs = cv2.dnn.NMSBoxes(boxes, confidences, self.score_threshold, self.overlap_threshold)

        objs = []
        if len(idxs) > 0:
            for i in idxs.flatten():
                (x, y, w, h) = boxes[i]

                objs.append(
                    {KEY_LABEL: LABELS[label_ids[i]],
                     KEY_CONFIDENCE: confidences[i],
                     KEY_BOX: (
                         np.divide(np.array([x, y, x + w, y + h]), np.array([img_w, img_h, img_w, img_h]))).astype(
                         np.float)
                     }
                )

        return objs
