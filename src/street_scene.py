import os
import cv2

from src.yolo_utils import YoloUtils
from src.plate_detect import PlateDetect
from src.face_utils import FaceUtils
from src.blur_utils import blur

from src.settings import C_PERSON
from src.draw_utils import DrawUtils


_cur_dir = os.path.dirname(os.path.realpath(__file__))
print("cur dir", _cur_dir)

if __name__ == '__main__':
    plate_detector = PlateDetect()
    person_detector = YoloUtils(targets=C_PERSON)
    face_detector = FaceUtils(mode="HOG")
    drawer = DrawUtils()

    # plate_img = cv2.imread("../data/vehicle.jpg")
    # plates = plate_detector.detect(img=plate_img)
    # # show_img = drawer.show_objects(img=plate_img, objects=plates)
    # show_img = blur(img=plate_img, objects=plates, type="rect")
    # cv2.imwrite("plates.jpg", show_img)

    person_img = cv2.imread("../data/persons.png")
    persons = person_detector.detect(img=person_img)
    faces = face_detector.detect(img=person_img)
    # show_img = drawer.show_objects(img=show_img, objects=faces)
    show_img = blur(img=person_img, objects=faces, type="circle")
    cv2.imwrite("persons.jpg", show_img)


if __name__ == '__main__':
    pass
