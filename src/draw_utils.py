import cv2
import numpy as np
from src.settings import KEY_BOX, KEY_LABEL, KEY_CONFIDENCE


# draw utils
SHOW_IMG_WIDTH = 2000

# grid setting
GRID_LINE_COLOR = (200, 200, 200)
GRID_LINE_THICK = 3

COLR_RECT_FILL = (255, 125, 0)
COLR_RECT_BORDER = (0, 0, 255)
COLR_TEXT = (255, 255, 0)


class DrawUtils:
    def __init__(self):
        pass

    @staticmethod
    def show_objects(img, objects):
        show_img = img.copy()
        img_h, img_w = img.shape[:2]

        for obj in objects:
            (x, y, x2, y2) = (obj[KEY_BOX] * np.array([img_w, img_h, img_w, img_h])).astype(np.int)
            label = obj[KEY_LABEL]

            # confidence = float(obj[KEY_CONFIDENCE])
            # str_label = "{}: {:.1f}%".format(label, confidence * 100)
            str_label = "{}".format(label)

            cv2.rectangle(show_img, (x, y), (x2, y2), COLR_RECT_BORDER, 3)
            cv2.putText(show_img, str_label, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.7, COLR_TEXT, 2)

        return show_img

    @staticmethod
    def show_roi(img, roi, color=(0, 255, 0)):
        bck = np.zeros_like(img)
        cv2.drawContours(bck, [roi], -1, color, -1)
        return cv2.addWeighted(img, 0.8, bck, 0.2, 0)
